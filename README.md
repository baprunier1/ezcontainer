# EZContainer

A simple injection container

## Configuration

To know which data type to inject, this container needs to be configured. This can be done either by
 setting a configuration file or manually configurating it.

### Configuration file

For the container to know what to inject, you need to use a configuration file. Its
format needs to be as follow :
```xml
<?xml version="1.0" encoding="utf-8" ?>
<configuration>
    <!-- dependencies -->
    <injections>
        <!-- using package tag -->
        <package name="fr.ezcorp.sample.no_injection">
            <inject class="SubAbstract" as="Concrete"/>
            <inject class="AbstractValue" as="ConcreteValue"/>
        </package>

        <!-- using package in string directly -->
        <inject class="fr.ezcorp.sample.no_injection.AbstractValue" as="fr.ezcorp.sample.injection.ConcreteValue2" id="two"/>
    </injections>

    <!-- using package name in injections tag -->
    <injections name="fr.ezcorp.sample.no_injection.">
        <inject class="Abstract" as="SubAbstract"/>
    </injections>



    <!-- values -->
    <values>
        <!-- using package tag -->
        <package name="fr.ezcorp.sample.no_injection">
            <inject class="ConcreteValue" as="5"/>
        </package>

        <!-- using package in string directly -->
        <inject class="java.lang.Integer" as="5"/>
    </values>

    <!-- using package name in values tag -->
    <values name="java.lang">
        <inject class="String" as="10" id="two"/>
        <inject class="String" as="100" id="three"/>
    </values>
</configuration>
```
You can inject either `values` or other class. In this example, `Abstract` is an
interface and it should be injected with a `SubAbstract`. But as this class also
needs to be injected, the `Abstract` variable will be injected with a `Concrete` 
class.  
Values are all represented as `String` so for a class to be injectable with a value,
it needs to have a constructor taking one and only one `String`.  
You will just need to instantiate your container as follow:
```java
// ResourcesHandler.getFilePath correspond to the resource manager of your project
EZContainer container = new EZContainer(new XMLDependencyLoader(ResourcesHandler.getFilePath("configuration.xml")));
```

### Manual configuration
The exact same configuration can be done manually, once the container is created with either `String` 
or `Class` items:
* Configuration with `String`:
```java
EZContainer manualContainer = new EZContainer();

// container configuration
manualContainer.setPackage("fr.ezcorp.sample.no_injection");
manualContainer.set("SubAbstract").as("Concrete");
manualContainer.set("AbstractValue").as("ConcreteValue");
manualContainer.set("Abstract").as("SubAbstract");
manualContainer.set("ConcreteValue").asValue("5");

manualContainer.setPackage("");
manualContainer.set("fr.ezcorp.sample.no_injection.AbstractValue")
               .id("two")
               .as("fr.ezcorp.sample.injection.ConcreteValue2");

manualContainer.setPackage("java.lang.");
manualContainer.set("Integer").asValue("5");
manualContainer.set("String").id("two").asValue("10");
manualContainer.set("String").id("three").asValue("100");
```
* Configuration with `Class`:
```java
EZContainer manualContainer = new EZContainer();

// manual configuration
manualContainer.set(SubAbstract.class).as(Concrete.class);
manualContainer.set(AbstractValue.class).as(ConcreteValue.class);
manualContainer.set(Abstract.class).as(SubAbstract.class);
manualContainer.set(ConcreteValue.class).asValue("5");

manualContainer.set(AbstractValue.class).id("two").as(ConcreteValue2.class);

manualContainer.set(Integer.class).asValue("5");
manualContainer.set(String.class).id("two").asValue("10");
manualContainer.set(String.class).id("three").asValue("100");
```

## Injection rules

### Injection order

1. Constructor (only one will be applied)
1. Attributes
1. Methods

### Auto Wiring
Auto wiring can be enabled thanks to method `setAutoWiring(boolean enableAutoWiring)`.
Once it is done, two kinds of components will be auto wired :
* **Attributes :** the attribute name needs to be the same as the class name you want to
inject in it but with its first character as lowercase
* **Setters :** the setter name needs to be `set` plus the attribute name capitalized 
to inject as its parameter
  
**Be careful :** to be injectable this way, the class needs to be in the configuration file at 
least once.

## How to use

### Initialize the container
The container needs to have an instance of `DependencyLoader` as its constructor 
parameter. This class defines the configuration file rules, so to use those explained 
in the [configuration file part](#markdown-header-configuration-file), you will need to 
use the `XMLDependencyLoader` class given with this tool. Here is an example of `EZContainer`
instantiation with a configuration file :
```java
// ResourcesHandler.getFilePath correspond to the resource manager of your project
EZContainer container = new EZContainer(new XMLDependencyLoader(ResourcesHandler.getFilePath("configuration.xml")));
```
More details can be found in the [Confugiration part](markdown-header-configuration).

### Declaring items as injectable
To tell the container which parts should be injected and how, you will need to use the 
`Inject` annotation given in this tool. Let's start with an example :
```java
package fr.ezcorp.sample;

import fr.ezcorp.Inject;

public class InjectEverywhere {
    @Inject
    public String s1;
    
    public String s2;
    public String s3;
    public String s4;
    public String s5;
    
    @Inject(id="three")
    public InjectEverywhere(String s1, @Inject(id="two") String s2){
        this.s2 = s1;
        this.s3 = s2;
    }
    
    @Inject(id="two")
    public void setStrings(@Inject(id="three") String s1, String s2){
        this.s4 = s1;
        this.s5 = s2;
    }
}
```
All attributes of this class are public for test purpose only, of course. The configuration
file used is the same as in the [configuration file part](#markdown-header-configuration-file).
You can see in this example that attributes, methods and constructors are injectable.
By default, the id given to a method / constructor is given when constructing its 
parameters, but you can change the id by specifically adding the annotation `Inject` 
to the parameter with its new id. The generation of this class would look like that :
```java
InjectEverywhere injected = container.generate(InjectEverywhere.class);
``` 
You can pass one more parameter to the `generate` method, which is an id if needed. If no 
id is specified (either in the configuration file or the annotation), the default constructor 
will be called.  
If the construction could not be done, one way or another, a `null` object is returned.