package fr.ezcorp;

import fr.ezcorp.sample.cyclic_dependency.AbstractAttributeDependency;
import fr.ezcorp.sample.cyclic_dependency.AbstractMethodDependency;
import fr.ezcorp.sample.cyclic_dependency.DirectCycle;
import fr.ezcorp.sample.injection.*;
import fr.ezcorp.sample.no_injection.*;
import fr.ezcorp.utils.StringTools;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestEZContainer extends Assert{
    private EZContainer container;

    @Before
    public void loadContainer(){
        container = EZContainer.generateFromXML(StringTools.getFilePath("configuration.xml"));
    }

    @Test
    public void testAttributeInjection(){
        trueTestAttributeInjection(container);
    }
    private void trueTestAttributeInjection(EZContainer container){
        try{
            InjectAttr1 injected = container.generate(InjectAttr1.class);

            assertNotNull(injected);
            assertNotNull(injected.getE());
        }catch (EZContainer.CyclicDependency e){
            fail();
        }

        try{
            InjectAttr2 injected = container.generate(InjectAttr2.class);

            assertNotNull(injected);
            assertNotNull(injected.getI());
            assertEquals(5, injected.getI().intValue());
        }catch (EZContainer.CyclicDependency e){
            fail();
        }
    }

    @Test
    public void testConstructorInjection(){
        trueTestConstructorInjection(container);
    }
    private void trueTestConstructorInjection(EZContainer container){
        try{
            InjectConstructor1 injected = container.generate(InjectConstructor1.class);

            assertNotNull(injected);
            assertNotNull(injected.i);
            assertEquals(5, injected.i.intValue());
        }catch (EZContainer.CyclicDependency e){
            fail();
        }
    }

    @Test
    public void testMethodInjection(){
        trueTestMethodInjection(container);
    }
    private void trueTestMethodInjection(EZContainer container){
        try{
            InjectMethod1 injected = container.generate(InjectMethod1.class);

            assertNotNull(injected);
            assertNotNull(injected.s1);
            assertNotNull(injected.s2);
            assertNotNull(injected.ia);
            assertNotNull(injected.ia.getI());
            assertEquals(5, injected.ia.getI().intValue());
        }catch (EZContainer.CyclicDependency e){
            fail();
        }
    }

    @Test
    public void testSingleton(){
        trueTestSingleton(container);
    }
    private void trueTestSingleton(EZContainer container){
        try {
            InjectConstructor1 injected = container.generateSingleton(InjectConstructor1.class);

            assertNotNull(injected);
            assertEquals(injected, container.generateSingleton(InjectConstructor1.class));
        }catch (EZContainer.CyclicDependency e){
            fail();
        }
    }

    @Test
    public void testInjectionId(){
        trueTestInjectionId(container);
    }
    private void trueTestInjectionId(EZContainer container){
        try {
            AbstractValue injected = container.generate(AbstractValue.class, "two");

            assertNotNull(injected);
            assertEquals("10", injected.get());

            injected = container.generate(AbstractValue.class);

            assertNotNull(injected);
            assertEquals("5", injected.get());
        }catch (EZContainer.CyclicDependency e){
            fail();
        }
    }

    @Test
    public void testAutoWiringAttribute(){
        trueTestAutoWiringAttribute(container);
    }
    private void trueTestAutoWiringAttribute(EZContainer container){
        container.setAutoWiring(true);

        try {
            InjectAutoWiringAttr injected = container.generate(InjectAutoWiringAttr.class);

            assertNotNull(injected);
            assertEquals("", injected.string);
        }catch (EZContainer.CyclicDependency e){
            fail();
        }finally {
            container.setAutoWiring(false);
        }

    }

    @Test
    public void testAutoWiringSetters(){
        trueTestAutoWiringSetters(container);
    }
    private void trueTestAutoWiringSetters(EZContainer container){
        container.setAutoWiring(true);

        try {
            InjectAutoWiringMethod injected = container.generate(InjectAutoWiringMethod.class);

            assertNotNull(injected);
            assertNotNull(injected.i);
            assertEquals(5, injected.i.intValue());
        }catch (EZContainer.CyclicDependency e){
            fail();
        }finally {
            container.setAutoWiring(false);
        }
    }

    @Test
    public void testInjectMethodParameterDifferentID(){
        trueTestInjectMethodParameterDifferentID(container);
    }
    private void trueTestInjectMethodParameterDifferentID(EZContainer container){
        try{
            InjectMethod2 injected = container.generate(InjectMethod2.class);

            assertNotNull(injected);
            assertEquals("100", injected.s);
        }catch (EZContainer.CyclicDependency e){
            fail();
        }
    }

    @Test
    public void testInjectConstructorParameterDifferentID(){
        trueTestInjectConstructorParameterDifferentID(container);
    }
    private void trueTestInjectConstructorParameterDifferentID(EZContainer container){
        try {
            InjectConstructor2 injected = container.generate(InjectConstructor2.class);

            assertNotNull(injected);
            assertEquals("100", injected.s);
        }catch (EZContainer.CyclicDependency e){
            fail();
        }
    }

    @Test
    public void testInjectEverywhere(){
        trueTestInjectEverywhere(container);
    }
    private void trueTestInjectEverywhere(EZContainer container){
        try {
            InjectEverywhere injected = container.generate(InjectEverywhere.class);

            assertNotNull(injected);
            assertEquals("", injected.s1);
            assertEquals("100", injected.s2);
            assertEquals("10", injected.s3);
            assertEquals("100", injected.s4);
            assertEquals("10", injected.s5);
        }catch (EZContainer.CyclicDependency e){
            fail();
        }
    }

    @Test
    public void testCyclicDependency(){
        trueTestCyclicDependency(container);
    }
    private void trueTestCyclicDependency(EZContainer container){
        try{
            container.generate(DirectCycle.class);
            fail();
        }catch (EZContainer.CyclicDependency e){}

        try{
            container.generate(AbstractAttributeDependency.class);
            fail();
        }catch (EZContainer.CyclicDependency e){}

        try {
             container.generate(AbstractMethodDependency.class);
             fail();
        }catch (EZContainer.CyclicDependency e){}
    }

    @Test
    public void testManualConfigWithString() {
        EZContainer manualContainer = new EZContainer();

        // container configuration
        manualContainer.setPackage("fr.ezcorp.sample.no_injection");
        manualContainer.set("SubAbstract").as("Concrete");
        manualContainer.set("AbstractValue").as("ConcreteValue");
        manualContainer.set("Abstract").as("SubAbstract");
        manualContainer.set("ConcreteValue").asValue("5");

        manualContainer.setPackage("");
        manualContainer.set("fr.ezcorp.sample.no_injection.AbstractValue")
                .id("two")
                .as("fr.ezcorp.sample.injection.ConcreteValue2");

        manualContainer.setPackage("java.lang.");
        manualContainer.set("Integer").asValue("5");
        manualContainer.set("String").id("two").asValue("10");
        manualContainer.set("String").id("three").asValue("100");

        // tests call
        trueTestAttributeInjection(manualContainer);
        trueTestConstructorInjection(manualContainer);
        trueTestMethodInjection(manualContainer);
        trueTestSingleton(manualContainer);
        trueTestInjectionId(manualContainer);
        trueTestAutoWiringAttribute(manualContainer);
        trueTestAutoWiringSetters(manualContainer);
        trueTestInjectMethodParameterDifferentID(manualContainer);
        trueTestInjectConstructorParameterDifferentID(manualContainer);
        trueTestInjectEverywhere(manualContainer);
        trueTestCyclicDependency(manualContainer);
    }

    @Test
    public void testManualConfigWithClass(){
        EZContainer manualContainer = new EZContainer();

        // manual configuration
        manualContainer.set(SubAbstract.class).as(Concrete.class);
        manualContainer.set(AbstractValue.class).as(ConcreteValue.class);
        manualContainer.set(Abstract.class).as(SubAbstract.class);
        manualContainer.set(ConcreteValue.class).asValue("5");

        manualContainer.set(AbstractValue.class).id("two").as(ConcreteValue2.class);

        manualContainer.set(Integer.class).asValue("5");
        manualContainer.set(String.class).id("two").asValue("10");
        manualContainer.set(String.class).id("three").asValue("100");

        // tests call
        trueTestAttributeInjection(manualContainer);
        trueTestConstructorInjection(manualContainer);
        trueTestMethodInjection(manualContainer);
        trueTestSingleton(manualContainer);
        trueTestInjectionId(manualContainer);
        trueTestAutoWiringAttribute(manualContainer);
        trueTestAutoWiringSetters(manualContainer);
        trueTestInjectMethodParameterDifferentID(manualContainer);
        trueTestInjectConstructorParameterDifferentID(manualContainer);
        trueTestInjectEverywhere(manualContainer);
        trueTestCyclicDependency(manualContainer);
    }
}
