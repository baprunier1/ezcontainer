package fr.ezcorp;

import fr.ezcorp.dependency.DependencyHandler;
import fr.ezcorp.dependency.DependencyLoader;
import fr.ezcorp.dependency.XMLDependencyLoader;
import fr.ezcorp.utils.StringTools;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class TestDependencyLoader extends Assert {

    @Test
    public void testXMLLoader(){
        DependencyLoader dependencyLoader = new XMLDependencyLoader(StringTools.getFilePath("configuration.xml"));
        DependencyHandler dependencyHandler = dependencyLoader.generateHandler();

        assertNotNull(dependencyHandler);

        // without id
        Map<String, String> dependencies = dependencyHandler.getDependencies("");

        assertEquals("fr.ezcorp.sample.no_injection.SubAbstract", dependencies.get("fr.ezcorp.sample.no_injection.Abstract"));
        assertEquals("fr.ezcorp.sample.no_injection.Concrete", dependencies.get("fr.ezcorp.sample.no_injection.SubAbstract"));

        Map<String, String> values = dependencyHandler.getValues("");

        assertEquals("5", values.get("java.lang.Integer"));

        // with id "two"
        dependencies = dependencyHandler.getDependencies("two");

        assertEquals("fr.ezcorp.sample.injection.ConcreteValue2", dependencies.get("fr.ezcorp.sample.no_injection.AbstractValue"));

        values = dependencyHandler.getValues("two");

        assertEquals("10", values.get("java.lang.String"));
    }
}
