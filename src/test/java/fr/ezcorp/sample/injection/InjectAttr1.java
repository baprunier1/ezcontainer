package fr.ezcorp.sample.injection;

import fr.ezcorp.Inject;

public class InjectAttr1 {
    @Inject
    private String e;

    public String getE(){
        return e;
    }
}
