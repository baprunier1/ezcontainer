package fr.ezcorp.sample.injection;

import fr.ezcorp.Inject;

public class InjectConstructor1 {
    public Integer i;

    @Inject
    public InjectConstructor1(Integer i){
        this.i = i;
    }
}
