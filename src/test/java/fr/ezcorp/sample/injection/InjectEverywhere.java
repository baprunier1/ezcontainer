package fr.ezcorp.sample.injection;

import fr.ezcorp.Inject;

public class InjectEverywhere {
    @Inject
    public String s1;

    public String s2;
    public String s3;
    public String s4;
    public String s5;

    @Inject(id="three")
    public InjectEverywhere(String s1, @Inject(id="two") String s2){
        this.s2 = s1;
        this.s3 = s2;
    }

    @Inject(id="two")
    public void setStrings(@Inject(id="three") String s1, String s2){
        this.s4 = s1;
        this.s5 = s2;
    }
}
