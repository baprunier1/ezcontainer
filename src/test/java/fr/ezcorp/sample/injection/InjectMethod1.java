package fr.ezcorp.sample.injection;

import fr.ezcorp.Inject;

public class InjectMethod1 {
    public String s1, s2;
    public InjectAttr2 ia;

    @Inject
    public void set1(String a, String b){
        s1 = a;
        s2 = b;
    }

    @Inject
    private void set2(InjectAttr2 ia){
        this.ia = ia;
    }
}
