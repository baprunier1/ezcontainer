package fr.ezcorp.sample.injection;

import fr.ezcorp.Inject;
import fr.ezcorp.sample.no_injection.AbstractValue;

public class ConcreteValue2 implements AbstractValue {
    @Inject(id="two")
    private String s;

    public String get() {
        return s;
    }
}
