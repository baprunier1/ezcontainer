package fr.ezcorp.sample.injection;

import fr.ezcorp.Inject;

public class InjectAttr2 {
    @Inject
    private Integer i;

    public Integer getI() {
        return i;
    }
}
