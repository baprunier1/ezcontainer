package fr.ezcorp.sample.injection;

import fr.ezcorp.Inject;

public class InjectConstructor2 {
    public String s;

    @Inject(id="two")
    public InjectConstructor2(@Inject(id="three") String s){
        this.s = s;
    }
}
