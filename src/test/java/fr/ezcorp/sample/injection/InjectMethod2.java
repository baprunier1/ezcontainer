package fr.ezcorp.sample.injection;

import fr.ezcorp.Inject;

public class InjectMethod2 {
    public String s;

    @Inject(id="two")
    void setString(@Inject(id="three") String s){
        this.s = s;
    }
}
