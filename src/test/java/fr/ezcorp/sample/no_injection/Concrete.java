package fr.ezcorp.sample.no_injection;

public class Concrete extends SubAbstract {
    public String get(){
        return this.getClass().getName();
    }
}
