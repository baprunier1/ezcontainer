package fr.ezcorp.sample.no_injection;

public interface Abstract {
    String get();
}
