package fr.ezcorp.sample.no_injection;

import fr.ezcorp.sample.no_injection.AbstractValue;

public class ConcreteValue implements AbstractValue {
    private String val;

    public ConcreteValue(String v){
        val = v;
    }

    public String get(){
        return val;
    }
}
