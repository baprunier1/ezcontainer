package fr.ezcorp.sample.no_injection;

public interface AbstractValue {
    String get();
}
