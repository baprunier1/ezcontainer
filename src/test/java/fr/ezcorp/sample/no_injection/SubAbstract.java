package fr.ezcorp.sample.no_injection;

import fr.ezcorp.sample.no_injection.Abstract;

public class SubAbstract implements Abstract {
    public String get(){
        return this.getClass().getName();
    }
}
