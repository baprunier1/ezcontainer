package fr.ezcorp.sample.cyclic_dependency;

import fr.ezcorp.Inject;

public class ConcreteMethodDependency {
    @Inject
    void loulou(AbstractMethodDependency amd){}
}
