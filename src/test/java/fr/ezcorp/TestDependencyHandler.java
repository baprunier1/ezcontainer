package fr.ezcorp;

import fr.ezcorp.dependency.DependencyHandler;
import fr.ezcorp.dependency.DependencyLoader;
import fr.ezcorp.dependency.XMLDependencyLoader;
import fr.ezcorp.sample.no_injection.Abstract;
import fr.ezcorp.sample.no_injection.Concrete;
import fr.ezcorp.sample.no_injection.SubAbstract;
import fr.ezcorp.utils.StringTools;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestDependencyHandler extends Assert{
    private DependencyHandler dependencyHandler;

    @Before
    public void loadHandler(){
        String filePath = StringTools.getFilePath("configuration.xml");
        DependencyLoader dependencyLoader = new XMLDependencyLoader(filePath);
        dependencyHandler = dependencyLoader.generateHandler();
    }

    @Test
    public void testGetConcreteClass(){
        String s;

        s = dependencyHandler.getConcreteClass(Abstract.class, "");
        assertNotNull(s);
        assertEquals(Concrete.class.getName(), s);

        s = dependencyHandler.getConcreteClass(SubAbstract.class, "");
        assertNotNull(s);
        assertEquals(Concrete.class.getName(), s);

        s = dependencyHandler.getConcreteClass(Concrete.class, "");
        assertNull(s);
    }

    @Test
    public void testGetValue(){
        String value;

        value = dependencyHandler.getValue(Integer.class, "");
        assertEquals("5", value);
    }
}
