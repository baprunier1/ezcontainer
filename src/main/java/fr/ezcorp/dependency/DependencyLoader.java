package fr.ezcorp.dependency;

/**
 * Generic dependency loader which generate a dependency handler
 */
public interface DependencyLoader {
    /**
     * Dependency handler generator
     * @return the generated dependency handler
     */
    DependencyHandler generateHandler();
}
