package fr.ezcorp.dependency;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

/**
 * Dependency loader using an XML configuration file
 */
public class XMLDependencyLoader implements DependencyLoader {
    /**
     * name of the file path from which to load dependencies
     */
    private String filePath;

    /**
     * Constructor using the file path from which to load dependencies
     * @param fileToLoad : path of the configuration file (xml)
     */
    public XMLDependencyLoader(String fileToLoad){
        this.filePath = fileToLoad;
    }

    /**
     * Generate a dependency handler using the configuration file
     * @return a dependency handler generated with the configuration file
     */
    public DependencyHandler generateHandler(){
        DependencyHandler dependencyHandler = new DependencyHandler();

        File file = new File(filePath);
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
                .newInstance();
        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(file);
            NodeList injections = document.getElementsByTagName("inject");

            for(int i = 0; i < injections.getLength(); ++i){
                Element injection = (Element)injections.item(i);
                if(injection == null)
                    continue;

                String className = injection.getAttribute("class");
                String targetName = injection.getAttribute("as");
                String id = injection.getAttribute("id");

                if(id == null){
                    id = "";
                }

                Element parentNode = (Element)injection.getParentNode();
                if(parentNode != null){
                    String packageName = parentNode.getAttribute("name");

                    if(packageName == null){
                        packageName = "";
                    }else if(!packageName.equals("") && !packageName.endsWith(".")){
                        packageName += ".";
                    }

                    if(parentNode.getNodeName().equals("injections") || parentNode.getParentNode().getNodeName().equals("injections")){
                        dependencyHandler.addDependency(id, packageName + className, packageName + targetName);
                    }else if(parentNode.getNodeName().equals("values") || parentNode.getParentNode().getNodeName().equals("values")){
                        dependencyHandler.addValue(id, packageName + className, targetName);
                    }
                }
            }
        } catch (Exception e){
            System.out.println(e.getMessage());
            for(int i = 0; i < e.getStackTrace().length; ++i){
                System.out.println(e.getStackTrace()[i]);
            }
            dependencyHandler = null;
        }

        return dependencyHandler;
    }
}
