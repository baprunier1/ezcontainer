package fr.ezcorp.dependency;

import com.sun.istack.internal.NotNull;

import java.util.HashMap;
import java.util.Map;

/**
 * Dependency handler
 */
public class DependencyHandler {
    /**
     * Map of all dependencies regrouped by id
     */
    private Map<String, Map<String, String>> dependencies;
    /**
     * Map of all values regrouped by id
     */
    private Map<String, Map<String, String>> values;

    /**
     * Create a dependency handler with neither dependency nor value
     */
    public DependencyHandler(){
        dependencies = new HashMap<String, Map<String, String>>();
        values = new HashMap<String, Map<String, String>>();
    }

    /**
     * Utility function to add a specific combination of [id, class name, value / child class name] to either values or dependencies
     * @param map : either values or dependencies
     * @param id : id of the combination
     * @param key : class name of the combination
     * @param value : 'inject as' value or class name
     */
    static private void add(Map<String, Map<String, String>> map, String id, String key, String value){
        if(map.containsKey(id)){
            map.get(id).put(key, value);
        }else{
            Map<String, String> subMap = new HashMap<String, String>();
            subMap.put(key, value);
            map.put(id, subMap);
        }
    }

    /**
     * Utility function to find the value or dependency of a class name with a specific id
     * @param map : either values or dependencies
     * @param className : key in the sub map of either values or dependencies
     * @param id : id of the class name to find
     * @return the value or dependency of a class name with a specific id or with id="" if not found, or null if also not found
     */
    private static String genericFind(Map<String, Map<String, String>> map, String className, String id){
        if(map.containsKey(id)) {
            String subClass = map.get(id).get(className);

            if(subClass == null && map.containsKey("")){
                subClass = map.get("").get(className);
            }

            return subClass;
        }else if(map.containsKey("")){
            return map.get("").get(className);
        }
        return null;

    }

    /**
     * Find sub class of a specific class with a specific id
     * @param className : key in the sub map of dependencies
     * @param id : id of the class name to find
     * @return the dependency of a class name with a specific id or with id="" if not found, or null if also not found
     */
    private String findSubclass(String className, String id){
        return genericFind(dependencies, className, id);
    }

    /**
     * Find value of a specific class with a specific id
     * @param className : key in the sub map of values
     * @param id : id of the class name to find
     * @return the value of a class name with a specific id or with id="" if not found, or null if also not found
     */
    private String findValue(String className, String id){
        return genericFind(values, className, id);
    }

    /**
     * Function to add a specific combination of [id, class name, child class name] to dependencies
     * @param id : id of the combination
     * @param className : class name of the combination
     * @param targetName : 'inject as' class name
     */
    public void addDependency(String id, String className, String targetName) {
        add(dependencies, id, className, targetName);
    }

    /**
     * Function to add a specific combination of [id, class name, value] to values
     * @param id : id of the combination
     * @param className : class name of the combination
     * @param value : 'inject as' value
     */
    public void addValue(String id, String className, String value){
        add(values, id, className, value);
    }

    /**
     * Get all dependencies of a certain id
     * @param id : id to get from
     * @return all dependencies of a certain id
     */
    public Map<String, String> getDependencies(String id) {
        return dependencies.get(id);
    }

    /**
     * Get all values of a certain id
     * @param id : id to get from
     * @return all values of a certain id
     */
    public Map<String, String> getValues(String id) {
        return values.get(id);
    }

    /**
     * get all dependencies
     * @return all dependencies
     */
    public Map<String, Map<String, String>> getDependencies(){
        return dependencies;
    }

    /**
     * get all values
     * @return all values
     */
    public Map<String, Map<String, String>> getValues(){
        return values;
    }

    /**
     * Get the class to be instantiated as a specific one
     * @param c : specific class
     * @param id : id of the injection
     * @return the class name of the class to be instantiated instead of 'c'
     */
    public String getConcreteClass(Class<?> c, String id){
        String subclass = c.getName();
        String oldSubClass;

        do{
            oldSubClass = subclass;
            subclass = findSubclass(subclass, id);
        }while(subclass != null);

        if(oldSubClass.equals(c.getName())){
            oldSubClass = null;
        }

        return oldSubClass;
    }

    /**
     * Get the class to be instantiated as a specific one
     * @param className : specific class name
     * @param id : id of the injection
     * @return the class name of the class to be instantiated instead of 'c'
     */
    public String getConcreteClass(String className, String id){
        try {
            return getConcreteClass(Class.forName(className), id);
        }catch(Exception e){
            return null;
        }
    }

    /**
     * Get the value to be set as a specific class
     * @param c : specific class
     * @param id : id of the injection
     * @return the value to be set as 'c'
     */
    public String getValue(Class<?> c, String id){
        Class<?> concrete;

        try{
            String className = getConcreteClass(c, id);
            concrete = Class.forName(className);
        }catch (Exception e){
            concrete = null;
        }

        if(concrete == null){
            concrete = c;
        }

        return findValue(concrete.getName(), id);
    }

    /**
     * Get the value to be set as a specific class
     * @param className : specific class name
     * @param id : id of the injection
     * @return the value to be set as 'c'
     */
    public String getValue(String className, String id){
        try {
            return getValue(Class.forName(className), id);
        }catch(Exception e){
            return null;
        }
    }

    /**
     * Find the full class name of a class without the knowledge of its package
     * @param endClassName : name of the class without its package
     * @return the name of the class with its package if found, null if not
     */
    public String findClassName(@NotNull String endClassName){
        String classe = "." + endClassName;

        for(Map<String, String> map : values.values()){
            for(String className : map.keySet()){
                if(className.endsWith(classe)){
                    return className;
                }
            }
        }

        for(Map<String, String> map : dependencies.values()){
            for(String className : map.keySet()){
                if(className.endsWith(classe)){
                    return className;
                }
            }

            for(String className : map.values()){
                if(className.endsWith(classe)){
                    return className;
                }
            }
        }

        return endClassName;
    }
}
