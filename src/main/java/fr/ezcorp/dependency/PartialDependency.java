package fr.ezcorp.dependency;

import fr.ezcorp.utils.StringTools;

/**
 * Partial dependency used to store information about a dependency while it has not been set completely
 */
public class PartialDependency {
    /**
     * class name of the injectable class
     */
    private String parent;
    /**
     * package to be used for the 'inject as' class (if any)
     */
    private String packageName;
    /**
     * id of the injection
     */
    private String id;
    /**
     * dependency handler to add the dependency info to when it is set completely (ie as or asValue called)
     */
    private DependencyHandler dependencyHandler;

    /**
     * Constructor using a dependency handler, the injectable class name and the package name ("" if no package name)
     * @param dependencyHandler : the dependency handler to add the dependency to
     * @param parent : the injectable class name
     * @param packageName : the package to use when as will be called (if it is called and if packageName is not "")
     */
    public PartialDependency(DependencyHandler dependencyHandler, String parent, String packageName){
        this.dependencyHandler = dependencyHandler;
        this.parent = parent;
        this.packageName = packageName;
        this.id = "";
    }

    /**
     * set the id of the injection
     * @param id : new id of the injection
     * @return itself
     */
    public PartialDependency id(String id){
        this.id = id;
        return this;
    }

    /**
     * end the definition of the dependency and set the 'inject as' class name
     * @param child : class name of the 'inject as'
     */
    public void as(String child){
        this.dependencyHandler.addDependency(id, parent, StringTools.getTrueName(packageName, child));
    }

    /**
     * end the definition of the dependency and set the 'inject as' class
     * @param child : class of the 'inject as'
     */
    public void as(Class child){
        this.dependencyHandler.addDependency(id, parent, child.getName());
    }

    /**
     * end the definition of the dependency and set the 'inject as' value
     * @param value : value to be injected
     */
    public void asValue(String value){
        this.dependencyHandler.addValue(id, parent, value);
    }
}
