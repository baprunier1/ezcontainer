package fr.ezcorp.utils;

import com.sun.istack.internal.NotNull;
import fr.ezcorp.Inject;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;


/**
 * Reflection utilities to get information more easily
 */
public class ReflectionTools {
    private ReflectionTools(){}

    /**
     * Getter for the first annotation of type Inject in a list of annotations, if any
     * @param annotations : list of annotations
     * @return the annotation of type Inject if found, null if not
     */
    public static Annotation getInjectAnnotation(Annotation[] annotations){
        for(Annotation annotation : annotations){
            if(annotation.annotationType() == Inject.class){
                return annotation;
            }
        }
        return null;
    }

    /**
     * Find the class of an attribute knowing its name in a list of Field
     * @param fields : list of fields
     * @param attrName : name of the attribute to find
     * @return the class of the attribute if found, null if not
     */
    public static Class<?> findClassFromAttribute(@NotNull Field[] fields, @NotNull String attrName){
        attrName = attrName.substring(0, 1).toLowerCase() + attrName.substring(1);
        for(Field field : fields){
            if(field.getName().equals(attrName)){
                return field.getType();
            }
        }

        return null;
    }
}
