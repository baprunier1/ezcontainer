package fr.ezcorp.utils;

import java.net.URL;

/**
 * String manipulations utilities
 */
public class StringTools {
    private StringTools(){}

    /**
     * Getter for the concatenated version of packageName and name taking adding a '.' if packageName does not end with a '.'
     * @param packageName : name of the package ("" if not package)
     * @param name : name of the class (supposedly without package if packageName is not "")
     * @return the concatenated version of packageName and name
     */
    public static String getTrueName(String packageName, String name){
        String trueName = packageName;
        if (!packageName.endsWith(".") && packageName.length() > 0) {
            trueName += ".";
        }
        trueName += name;
        return trueName;
    }

    /**
     * Getter for the path of a file using getClassLoader utility (to find resource file)
     * @param fileName : file to get the path from
     * @return the path of a file
     */
    static public String getFilePath(String fileName){
        URL url = ResourcesHandler.class.getClassLoader().getResource(fileName);

        if(url == null){
            return null;
        }

        return url.getFile();
    }
}
