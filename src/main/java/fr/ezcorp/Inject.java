package fr.ezcorp;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Inject annotation used to know what can be injected
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Inject {
    /**
     * Getter for the id used for the injection
     * @return the id used for the injection
     */
    String id() default "";
}
