package fr.ezcorp;

import com.sun.istack.internal.NotNull;
import fr.ezcorp.dependency.DependencyHandler;
import fr.ezcorp.dependency.DependencyLoader;
import fr.ezcorp.dependency.PartialDependency;
import fr.ezcorp.dependency.XMLDependencyLoader;
import fr.ezcorp.utils.ReflectionTools;
import fr.ezcorp.utils.StringTools;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Injection container
 */
public class EZContainer {
    // ---------- intern classes
    /**
     * Exception thrown when a cyclic dependency occure while generating an object
     */
    public class CyclicDependency extends Exception {
        public CyclicDependency(){
            super();
        }
        public CyclicDependency(String s){
            super(s);
        }
    }



    // ---------- attributes
    /**
     * Saved package name used when manually setting dependencies
     */
    String packageName;
    /**
     * Dependency handler used to generate objects
     */
    private DependencyHandler dependencyHandler;
    /**
     * Singletons already instantiated
     */
    private Map<Class, Object> singletons;
    /**
     * flag to tells if auto wiring should be used
     */
    private boolean autoWiring;



    // ---------- static methods
    /**
     * Generate an EZContainer using a configuration file
     * @param configurationFilePath : path to the configuration file (xml)
     * @return an instance of EZContainer generated using the configuration file
     */
    public static EZContainer generateFromXML(@NotNull String configurationFilePath){
        return new EZContainer(new XMLDependencyLoader(configurationFilePath));
    }



    // ---------- Constructors
    /**
     * Generate an empty EZContainer (no dependency, no value)
     */
    public EZContainer(){
        this(new DependencyHandler());
    }

    /**
     * Generate an EZContainer using a dependency loader
     * @param dependencyLoader : dependency loader to use to generate the dependency handler
     */
    private EZContainer(@NotNull DependencyLoader dependencyLoader){
        this(dependencyLoader.generateHandler());
    }

    /**
     * Generate an EZContainer using a dependency handler
     * @param dependencyHandler : dependency handler used for the container
     */
    private EZContainer(@NotNull DependencyHandler dependencyHandler){
        this.dependencyHandler = dependencyHandler;
        singletons = new HashMap<Class, Object>();
        setAutoWiring(false);
        setPackage("");
    }



    // ---------- setters

    /**
     * Initialize the creation of a dependency using package.parent as the class injectable
     * @param parent : name of the class injectable which will be concatenated with the package name set if any
     * @return the partial dependency created
     */
    public PartialDependency set(String parent){
        return new PartialDependency(dependencyHandler, StringTools.getTrueName(packageName, parent), packageName);
    }

    /**
     * Initialize the creation of a dependency using parent.getName() as the class injectable
     * @param parent : class injectable
     * @return the partial dependency created
     */
    public PartialDependency set(Class parent){
        return new PartialDependency(dependencyHandler, parent.getName(), packageName);
    }

    /**
     * Set package name to be used when creating dependencies
     * @param packageName : new package name, set to "" if no package wanted
     */
    public void setPackage(@NotNull String packageName){
        this.packageName = packageName;
    }

    /**
     * Enable / disable auto wiring
     * @param enableAutoWiring : flag which tells to enable / disable auto wiring
     */
    public void setAutoWiring(boolean enableAutoWiring){
        autoWiring = enableAutoWiring;
    }



    // ---------- other methods

    /**
     * Instantiate an object either using an injectable constructor, the value or the default constructor
     * @param classe : class of the object to be instantiated
     * @param value : value (if any) of the object to be instantiated
     * @param forbidden : list of all classes that are being constructed when constructing this one (to avoid cyclic dependencies)
     * @param <T> : type of the object to be instantiated
     * @return the object created, null if it failed
     * @throws CyclicDependency : this class is created in an object needed by an object needed ... which is this class
     */
    @SuppressWarnings("unchecked")
    private <T> T instantiate(@NotNull Class<T> classe, String value, List<String> forbidden)
            throws CyclicDependency{
        T object = null;

        try {
            if (value == null) {
                Constructor[] constructors = classe.getConstructors();

                if (constructors != null && constructors.length > 0) {
                    for (Constructor constructor : constructors) {
                        Inject annotation = (Inject) constructor.getAnnotation(Inject.class);

                        if (annotation != null) {
                            object = generateWithConstructor(classe, constructor, annotation.id(), forbidden);
                        }
                    }
                }
            } else {
                Constructor constructor = classe.getConstructor(String.class);

                if (constructor != null) {
                    return (T)(constructor.newInstance(value));
                }
            }

            if (object == null) {
                object = classe.newInstance();
            }
        }catch(CyclicDependency e){
            throw e;
        }catch (Exception e){
            object = null;
        }

        return object;
    }

    /**
     *
     * @param classe : class of the object to be instantiated
     * @param constructor : constructor from which the object needs to be instantiated
     * @param id : id of the injection to use
     * @param forbidden : list of all classes that are being constructed when constructing this one (to avoid cyclic dependencies)
     * @param <T> : type of the object to be instantiated
     * @return the object created, null if it failed
     * @throws CyclicDependency : this class is created in an object needed by an object needed ... which is this class
     * @throws IllegalAccessException : thrown by constructor.newInstance
     * @throws InvocationTargetException : thrown by constructor.newInstance
     * @throws InstantiationException : thrown by constructor.newInstance
     */
    @SuppressWarnings("unchecked")
    private <T> T generateWithConstructor(@NotNull Class<T> classe, @NotNull Constructor<?> constructor, String id, List<String> forbidden)
            throws CyclicDependency, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class[] types = constructor.getParameterTypes();
        Annotation[][] parameterAnnotations = constructor.getParameterAnnotations();
        List<Object> parameters = new ArrayList<Object>();

        constructor.setAccessible(true);

        for(int i = 0; i < types.length && i < parameterAnnotations.length; ++i){
            String trueId = id;
            if(types[i] == classe){
                throw new CyclicDependency("Recursive constructor");
            }
            Inject annotation = (Inject)ReflectionTools.getInjectAnnotation(parameterAnnotations[i]);
            if(annotation != null) {
                trueId = annotation.id();
            }

            parameters.add(generate(types[i], trueId, new ArrayList<String>(forbidden)));
        }

        return (T)(constructor.newInstance(parameters.toArray()));
    }

    /**
     * Generate an instance of 'classe' and inject what can be injected inside (see generate(Class, String, List) for more details)
     * @param classe : class of the object to be instantiated
     * @param <T> : type of the object to be instantiated
     * @return the object created, null if it failed
     * @throws CyclicDependency : this class is created in an object needed by an object needed ... which is this class
     */
    public <T> T generate(@NotNull Class<T> classe) throws CyclicDependency{
        return generate(classe, "");
    }

    /**
     * Generate an instance of 'classe' with a specific id and inject what can be injected inside (see generate(Class, String, List) for more details)
     * @param classe : class of the object to be instantiated
     * @param id : id of the injection to use
     * @param <T> : type of the object to be instantiated
     * @return the object created, null if it failed
     * @throws CyclicDependency : this class is created in an object needed by an object needed ... which is this class
     */
    public <T> T generate(@NotNull Class<T> classe, String id) throws CyclicDependency{
        return generate(classe, id, new ArrayList<String>());
    }

    /**
     * Generate an instance of 'classe' with a specific id, handle cyclic dependencies and inject what can be injected inside
     * @param classe : class of the object to be instantiated
     * @param id : id of the injection to use
     * @param forbidden : list of all classes that are being constructed when constructing this one (to avoid cyclic dependencies)
     * @param <T> : type of the object to be instantiated
     * @return the object created, null if it failed
     * @throws CyclicDependency : this class is created in an object needed by an object needed ... which is this class
     */
    @SuppressWarnings("unchecked")
    private <T> T generate(@NotNull Class<T> classe, String id, List<String> forbidden) throws CyclicDependency{
        if(forbidden.contains(classe.getName())){
            throw new CyclicDependency();
        }else{
            forbidden.add(classe.getName());
        }

        try {
            // generating son class instead if any
            String className = dependencyHandler.getConcreteClass(classe, id);
            Class<?> concrete = null;
            if(className != null) {
                concrete = Class.forName(className);
            }
            String value;

            if (concrete == null) {
                value = dependencyHandler.getValue(classe, id);
            } else {
                value = dependencyHandler.getValue(concrete, id);
            }
            if (concrete != null && value == null) {
                return (T) (generate(concrete, id, forbidden));
            }
            Class toUseClass = classe;
            if (concrete != null) {
                toUseClass = concrete;
            }

            // generating from default value if primitive type
            if (toUseClass.isPrimitive()) {
                return (T) (toUseClass.newInstance());
            }

            // Constructing object
            T object;
            object = (T) (instantiate(toUseClass, value, forbidden));

            if (object != null) {
                // Generate injectable attributes
                generateFields(object, toUseClass, id, forbidden);

                // Call injectable methods
                generateMethods(object, toUseClass, id, forbidden);
            }

            return object;
        }catch (CyclicDependency e){
            throw e;
        }catch (Exception e){
            e.getStackTrace();
        }

        return null;
    }

    /**
     * Inject all injectable fields (or all fields if auto wiring is on) of an object
     * @param object : object to inject its fields
     * @param classe : class of the object
     * @param id : id of the injection to use
     * @param forbidden : list of all classes that are being constructed when constructing this one (to avoid cyclic dependencies)
     * @throws CyclicDependency : this class is created in an object needed by an object needed ... which is this class
     */
    private void generateFields(Object object, Class<?> classe, String id, List<String> forbidden)
            throws CyclicDependency{
        Field[] fields = classe.getDeclaredFields();

        for (Field field : fields) {
            Inject annotation = field.getAnnotation(Inject.class);

            if (annotation != null) {
                field.setAccessible(true);
                try {
                    field.set(object, generate(field.getType(), annotation.id(), new ArrayList<String>(forbidden)));
                }catch(IllegalAccessException e){
                    object = null;
                }
            }else if(autoWiring){
                try {
                    String fieldName = field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);
                    Class fieldClass = Class.forName(dependencyHandler.findClassName(fieldName));

                    Object fieldValue = generate(fieldClass, id, new ArrayList<String>(forbidden));

                    if (fieldValue != null) {
                        field.setAccessible(true);
                        try {
                            field.set(object, fieldValue);
                        }catch(IllegalAccessException e){
                            object = null;
                        }
                    }
                }catch(CyclicDependency e){
                    throw e;
                }catch(Exception e){
                    object = null;
                }
            }
        }
    }

    /**
     * Inject all injectable methods (or all setters if auto wiring is on) of an object
     * @param object : object to inject its methods
     * @param classe : class of the object
     * @param id : id of the injection to use
     * @param forbidden : list of all classes that are being constructed when constructing this one (to avoid cyclic dependencies)
     * @throws InvocationTargetException : thrown by Method.invoke
     * @throws CyclicDependency : this class is created in an object needed by an object needed ... which is this class
     */
    private void generateMethods(Object object, Class<?> classe, String id, List<String> forbidden)
            throws InvocationTargetException, CyclicDependency{
        Method[] methods = classe.getDeclaredMethods();

        for(Method method : methods){
            Inject annotation = method.getAnnotation(Inject.class);

            if(annotation != null){
                ArrayList<Object> parameters = new ArrayList<Object>();
                Class[] parameterClasses = method.getParameterTypes();
                Annotation[][] parameterAnnotations = method.getParameterAnnotations();

                for(int i = 0; i < parameterClasses.length && i < parameterAnnotations.length; ++i){
                    String trueId = annotation.id();
                    Inject annotationParameter = (Inject) ReflectionTools.getInjectAnnotation(parameterAnnotations[i]);
                    if(annotationParameter != null){
                        trueId = annotationParameter.id();
                    }

                    parameters.add(generate(parameterClasses[i], trueId, new ArrayList<String>(forbidden)));
                }

                method.setAccessible(true);
                try {
                    method.invoke(object, parameters.toArray());
                }catch (IllegalAccessException e){
                    object = null;
                }
            }else if(autoWiring && method.getName().startsWith("set")){
                try {
                    Class<?> attribClass = ReflectionTools.findClassFromAttribute(classe.getDeclaredFields(), method.getName().substring(3));
                    if (attribClass != null) {
                        Object parameter = generate(attribClass, id, new ArrayList<String>(forbidden));

                        method.setAccessible(true);
                        method.invoke(object, parameter);
                    }
                }catch(CyclicDependency e){
                    throw e;
                }catch(Exception e){
                    object = null;
                }
            }
        }
    }

    /**
     * Generate or get a singleton to a specific class and inject what can be injected inside (see generate(Class, String, List) for more details)
     * @param classe : class of the object to be instantiated
     * @param <T> : type of the object to be instantiated
     * @return the object created (only once), null if it failed
     * @throws CyclicDependency : this class is created in an object needed by an object needed ... which is this class
     */
    public <T> T generateSingleton(@NotNull Class<T> classe) throws CyclicDependency{
        return generateSingleton(classe, "");
    }

    /**
     * Generate or get a singleton to a specific class with a specific id and inject what can be injected inside (see generate(Class, String, List) for more details)
     * @param classe : class of the object to be instantiated
     * @param id : id of the injection to use
     * @param <T> : type of the object to be instantiated
     * @return the object created (only once), null if it failed
     * @throws CyclicDependency : this class is created in an object needed by an object needed ... which is this class
     */
    @SuppressWarnings("unchecked")
    public <T> T generateSingleton(@NotNull Class<T> classe, String id) throws CyclicDependency{
        if(singletons.containsKey(classe)){
            return (T)(singletons.get(classe));
        }
        try {
            T o = generate(classe, id);

            if (o != null) {
                singletons.put(classe, o);
            }

            return o;
        }catch(CyclicDependency e){
            throw e;
        }catch (Exception e){
            return null;
        }
    }
}
